//
//  BottomBarSegmentedControl.swift
//  KiteUI
//
//  Created by Matteo Koczorek on 27.10.14.
//  Copyright (c) 2014 Matteo Koczorek. All rights reserved.
//

import UIKit

class BottomBarSegmentedControlSegment: UIView {
    
    var label: UILabel!
    var imageView: UIImageView!
    
    
    // MARK: - Text
    var text: String? {
        get {
            return self.label.text
        }
        set {
            self.label?.text = newValue
        }
    }
    
    var textColor: UIColor {
        get {
            return self.label.textColor
        }
        set {
            self.label?.textColor = newValue
        }
    }
    
    // MARK: - Image
    var image: UIImage? {
        didSet {
            self.imageView?.image = image
        }
    }
    
    override var frame: CGRect {
        didSet {
            maintainCopies()
        }
    }
    var copies: [BottomBarSegmentedControlSegment] = []
    
    // MARK: - Init
    init() {
        super.init(frame: CGRectZero)
        construct()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        construct()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        construct()
    }
    
    
    // MARK: - Setup
    func construct() {
        self.setupLabel()
        self.setupImageView()
        //self.backgroundColor = UIColor.yellowColor()
    }
    
    func setupLabel() {
        self.label = UILabel()
        self.addSubview(self.label)
        self.label.textAlignment = NSTextAlignment.Center
        self.label.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.5)
    }
    
    func setupImageView() {
        self.imageView = UIImageView()
        self.addSubview(self.imageView)
    }
    
    // MARK: - UIView
    override func removeFromSuperview() {
        super.removeFromSuperview()
        for copy in copies {
            copy.removeFromSuperview()
        }
    }
    
    // MARK: - Maintained Copy
    func getMaintainedCopy() -> BottomBarSegmentedControlSegment {
        let copy = BottomBarSegmentedControlSegment()
        copyParameters(copy)
        copies.append(copy)
        return copy
    }
    
    func copyParameters(toSegment: BottomBarSegmentedControlSegment) {
        toSegment.frame = self.frame
        toSegment.label?.text = self.label?.text
        toSegment.label?.font = self.label?.font
        toSegment.image = self.image
    }
    func maintainCopies() {
        for copy in copies {
            copyParameters(copy)
        }
    }
    
    // MARK: - Layout
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layout()
    }
    
    func layout() {
        self.layoutLabel()
        self.layoutImageView()
        self.maintainCopies()
    }
    
    func layoutLabel() {
        self.label?.frame.size = self.frame.size
    }
    
    func layoutImageView() {
        if self.imageView.image == nil {
            return
        }
        let image = self.imageView.image!
        let selfSize = self.frame.size
        self.imageView.frame = CGRect(
            x: selfSize.width/2 - image.size.width/2,
            y: selfSize.height/2 - image.size.height/2,
            width: image.size.width,
            height: image.size.height
        )
    }
    
}

@objc protocol BottomBarSegmentedControlDelegate {
    optional func bottomBarSegmentedControlShouldAnimateSegmentChange(bottomBarSegmentedControl:BottomBarSegmentedControl) -> Bool
}

class BottomBarSegmentedControl: UIControl {
    
    //exposed properties
    
    weak var delegate: BottomBarSegmentedControlDelegate?
    
    var font: UIFont = UIFont.systemFontOfSize(18) {
        didSet {
            for segment in self.segments {
                segment.label.font = self.font
            }
        }
    }
    private var _selectedSegmentIndex:NSInteger = 0
    var selectedSegmentIndex: NSInteger {
        get {
            return _selectedSegmentIndex
        }
        set {
            _selectedSegmentIndex = newValue
            self.sendActionsForControlEvents(.ValueChanged)
            snap()
        }
    }
    
    var shouldAnimateSegmentChange: Bool {
        get {
            var delShould = self.delegate?.bottomBarSegmentedControlShouldAnimateSegmentChange?(self)
            return delShould != nil ? delShould! : true
        }
    }
    
    var titles: [String] = [] {
        didSet {
            calculateMargin()
            setupSegments()
        }
    }
    var images: [UIImage] = [] {
        didSet {
            calculateMargin()
            setupSegments()
        }
    }
    var margin: CGFloat = 10.0
    
    
    private var segments: [BottomBarSegmentedControlSegment] = []
    let gradientMask: UIView = UIView()
    private var gradient = UIImageView()
    let gradientContainer = UIView()
    let indicator = UIView()
    private var percentMoved: CGFloat = 0
    
    
    //public methods
    func moveToPercent(percent:CGFloat) {
        percentMoved = percent
        let distanceToMove = self.frame.size.width  - gradient.frame.size.width
        gradient.frame.origin.x = distanceToMove * percent
        indicator.frame.origin.x = margin + distanceToMove * percent
        //indicator.frame.origin.x =  distanceToMove * percent
        _selectedSegmentIndex = self.segmentIndexForOffset(percentMoved * self.frame.size.width)
    }
    
    //initializers
    init() {
        super.init(frame: CGRectZero)
        construct()
    }
    
    init(titles: [String]) {
        super.init(frame: CGRectZero)
        self.titles = titles
        construct()
    }
    
    init(images: [UIImage]) {
        super.init(frame: CGRectZero)
        self.images = images
        construct()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        construct()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        construct()
    }
    
    private func construct() {
        setup()
    }
    
    
    
    /*private func calculateMargin() {
        let segment = BottomBarSegmentedControlSegment()
        var maxWidth = 0 as CGFloat
        for title in titles {
            let size = (title as NSString).sizeWithAttributes([NSFontAttributeName: segment.label.font])
            if(size.width > maxWidth) {
                maxWidth = size.width
            }
        }
        maxWidth += 8
        let segmentsWidth = maxWidth * CGFloat(titles.count)
        let delta: CGFloat =  self.frame.size.width - segmentsWidth
        margin = delta / (CGFloat(titles.count) + 1.0)
    }*/
    private func calculateMargin() {
        var maxWidth: CGFloat = 0
        for (index,segment) in enumerate(self.segments) {
            var size: CGSize = CGSizeZero
            if index < self.images.count {
                size = self.images[index].size
            }
            else {
                var title = self.titles[index]
                //->
                if segment.label != nil {
                    size = (title as NSString).sizeWithAttributes([NSFontAttributeName: segment.label.font])
                }
            }
            if(size.width > maxWidth) {
                maxWidth = size.width
            }
        }
        maxWidth += 8
        let segmentsWidth = maxWidth * CGFloat(self.numberOfSegments())
        let delta: CGFloat =  self.frame.size.width - segmentsWidth
        margin = delta / (CGFloat(self.numberOfSegments()) + 1.0)
    }
    
    // MARK: - Setup
    private func setup() {
        setupGradient()
        setupGradientMask()
        setupIndicator()
        setupSegments()
    }
    
    func setupGradient() {
        gradient.image = UIImage(named: "BBSC_HighlightGradient.png")
        gradient.frame.origin.x = 0
        addSubview(gradientContainer)
        gradientContainer.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.01)
        gradientContainer.addSubview(gradient)
        gradientContainer.userInteractionEnabled = false
    }
    
    func setupGradientMask() {
        addSubview(gradientMask)
        //gradientMask.backgroundColor = UIColor.redColor()
        gradientContainer.layer.mask = gradientMask.layer
        gradientMask.userInteractionEnabled = false
    }
    
    func setupIndicator() {
        indicator.backgroundColor = UIColor.whiteColor()
        self.addSubview(indicator)
    }
    
    private func setupSegments() {
        clearSegments()
        createSegments()
    }
    
    // MARK: - Segments
    private func clearSegments() {
        for segment in segments {
            segment.removeFromSuperview()
        }
        segments = []
    }
    
    /*private func addSegment(title: String) {
        //create segment
        let segment = BottomBarSegmentedControlSegment()
        segment.text = title
        //addSubview(segment)
        segment.alpha = 1
        segment.label.font = self.font
        insertSubview(segment, belowSubview: gradientContainer)
        //create maintained copy
        let copy = segment.getMaintainedCopy()
        copy.textColor = UIColor.whiteColor()
        gradientMask.addSubview(copy)
        
        segments.append(segment)
    }
    
    private func addSegment(image: UIImage) {
        //create segment
        let segment = BottomBarSegmentedControlSegment()
        segment.image = image
        //addSubview(segment)
        segment.alpha = 1
        segment.label.font = self.font
        insertSubview(segment, belowSubview: gradientContainer)
        //create maintained copy
        let copy = segment.getMaintainedCopy()
        copy.textColor = UIColor.whiteColor()
        gradientMask.addSubview(copy)
        
        segments.append(segment)
    }
    
    private func createSegments() {
        for title in titles {
            addSegment(title)
        }
        layoutSegments()
    }*/
    
    func createSegments() {
        for var i = 0; i < self.numberOfSegments(); i++ {
            var segment = self.segmentAtIndex(i)
            segment.userInteractionEnabled = false
            insertSubview(segment, belowSubview: gradientContainer)
            //create maintained copy
            let copy = segment.getMaintainedCopy()
            copy.textColor = UIColor.whiteColor()
            gradientMask.addSubview(copy)
            
            segments.append(segment)
        }
        self.calculateMargin()
        self.layout()
    }
    
    // MARK: - Helper
    
    private func calculatedSegmentSize() -> CGSize {
        if(segments.count <= 0) {
            return CGSizeZero
        }
        //the width for each individual segment minus the space left for margin
        let width = ((self.frame.size.width - margin) / CGFloat(self.segments.count)) - margin
        let height = 20.0 as CGFloat
        return CGSizeMake(width , height)
    }
    
    func numberOfSegments() -> NSInteger {
        return self.titles.count > self.images.count ? self.titles.count : self.images.count
    }
    
    func segmentAtIndex(index:NSInteger) -> BottomBarSegmentedControlSegment {
        let segment = BottomBarSegmentedControlSegment()
        if index < self.images.count {
            let image = self.images[index]
            segment.image = image
            segment.alpha = 0.5
        }
        else {
            let title = self.titles[index]
            segment.text = title
            segment.label?.font = self.font
        }
        return segment
    }
    
    // MARK: - Layout
    private func layoutSegments() {
        for (index, segment) in enumerate(segments) {
            let segmentSize = calculatedSegmentSize()
            let x = margin + CGFloat(index) * (segmentSize.width + margin)
            let y = CGFloat(0)
            let width = segmentSize.width
            let height = segmentSize.height
            segment.frame = CGRectMake(x, y, width, height)
        }
    }
    
    private func layoutGradient() {
        var gradientSize = calculatedSegmentSize()
        gradientSize.width += margin*2
        gradient.frame.size = gradientSize
        gradientContainer.frame.size = self.frame.size
    }
    
    private func layoutGradientMask() {
        gradientMask.frame.size = self.frame.size
    }
    
    private func layoutIndicator() {
        let segmentSize = calculatedSegmentSize()
        indicator.frame.size = CGSizeMake(segmentSize.width, 2)
        indicator.frame.origin.y = self.frame.size.height - indicator.frame.size.height
    }

    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layout()
    }
    
    func layout() {
        calculateMargin()
        layoutSegments()
        layoutGradient()
        layoutGradientMask()
        layoutIndicator()
        moveToPercent(percentMoved)
    }
    
    private func segmentIndexForOffset(offset:CGFloat) -> NSInteger {
        let segmentSize = calculatedSegmentSize()
        if(segmentSize.width == 0) {
            return 0
        }
        return NSInteger(offset / (segmentSize.width + margin*1.5))
    }
    
    //UI Actions
    func snap() {
        let percent = self.segments.count > 0 ? CGFloat(self.selectedSegmentIndex) / CGFloat(self.segments.count-1) : 0
        if self.shouldAnimateSegmentChange {
            UIView.animateWithDuration(0.4, delay: 0.0, options: UIViewAnimationOptions.CurveEaseInOut, animations: { () -> Void in
                self.moveToPercent(percent)
            }) { (completed) -> Void in
                
            }
        }
        else {
            self.moveToPercent(percent)
        }
    }
    
    // MARK: - UIControl
    override func beginTrackingWithTouch(touch: UITouch, withEvent event: UIEvent) -> Bool {
        return true
    }
    
    override func continueTrackingWithTouch(touch: UITouch, withEvent event: UIEvent) -> Bool {
        return true
    }
    
    override func endTrackingWithTouch(touch: UITouch, withEvent event: UIEvent) {
        let location = touch.locationInView(self)
        selectedSegmentIndex = segmentIndexForOffset(location.x)
    }
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
