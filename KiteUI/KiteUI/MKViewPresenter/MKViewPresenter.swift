//
//  MKViewPresenter.swift
//  Piles
//
//  Created by Matteo Koczorek on 20.12.14.
//  Copyright (c) 2014 Matteo Koczorek. All rights reserved.
//

import UIKit

enum MKViewPresenterPresentationStyle {
    case None, TopSheet
}

class MKViewPresenter: NSObject {
   
    var presentationStyle: MKViewPresenterPresentationStyle = .TopSheet
    
    override init() {
        super.init()
        self.setup()
    }
    
    func setup() {
        
    }
    
    func present(view: UIView) {
        
    }
    
    func window() -> UIWindow {
        return UIApplication.sharedApplication().delegate!.window!!
    }
}
