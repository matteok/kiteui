//
//  PullToPost.swift
//  KiteUI
//
//  Created by Matteo Koczorek on 14.09.14.
//  Copyright (c) 2014 Matteo Koczorek. All rights reserved.
//

import UIKit

@objc protocol PullToPostDataSource{

}

class PullToPostItem: UIView {
    var particle: CMTPParticle?
    var anchor: CMTPParticle?
    var attraction: CMTPAttraction?
    var spring: CMTPSpring?
    var highlighted: Bool = false {
        didSet {
            if(highlighted) {
                self.backgroundColor = UIColor(red: 2.0/255.0, green: 130.0/255.0, blue: 182.0/255.0, alpha: 1)
            }
            else {
                self.backgroundColor = UIColor(red: 218.0/255.0, green: 225.0/255.0, blue: 229.0/255.0, alpha: 1)
            }
        }
    }
    
    init() {
        super.init(frame: CGRectZero)
        construct()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        construct()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        construct()
    }
    
    func construct() {
        self.layer.cornerRadius = self.frame.size.width/2
    }
    
    //set the particles position as center
    func synchToParticlePosition() {
        
    }
}

class PullToPost: UIControl {
    
    var displayLink: CADisplayLink?
    
    var attractor: CMTPParticle?
    var particleSystem: CMTPParticleSystem?
    var activeItem: PullToPostItem?
    var items = NSMutableArray()
    var doTick = true
    var itemSize: CGFloat = 38.0
    var itemMargin: CGFloat = 16.0
    var selectedItemIndex:NSInteger = -1
    var shadowView: UIView = UIView()
    var markerView: UIView = UIView()
    var active: Bool = false {
        didSet {
            if(active) {
            }
            else {
                resetSprings()
                resetHighlights()
            }
        }
    }
    
    
    var scrollView: UIScrollView? {
        didSet {
            scrollView!.panGestureRecognizer.addTarget(self, action: "panRecognized:")
            scrollView!.addSubview(markerView)
        }
    }
    
    
    init() {
        super.init(frame: CGRectZero)
        construct()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //construct()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
       // construct()
    }
    
    func construct() {
        self.clipsToBounds = true
        displayLink = CADisplayLink(target:self, selector: "draw")
        displayLink!.addToRunLoop(NSRunLoop.mainRunLoop(), forMode: NSRunLoopCommonModes)
        setupLayout()
        setupShadowLayer()
        setupPhysics()
        addItems()
        
        markerView.backgroundColor = UIColor.redColor()
    }
    
    func setupLayout() {
        backgroundColor = UIColor(red: 184.0/255.0, green: 197.0/255.0, blue: 204.0/255.0, alpha: 1)
    }
    
    func setupShadowLayer() {
        shadowView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1).CGColor
        shadowView.layer.shadowOffset = CGSizeMake(2, 2)
        shadowView.layer.shadowRadius = 2
        shadowView.layer.shadowOpacity = 0.6
        shadowView.layer.backgroundColor = UIColor.whiteColor().CGColor
        addSubview(shadowView)
    }
    
    func setupPhysics() {
        let gravity = CMTPVector3DMake(0.0, 0.0, 0.0);
        particleSystem = CMTPParticleSystem(gravityVector: gravity, drag: 0.3)
        attractor = particleSystem!.makeParticleWithMass(1.0, position: CMTPVector3DMake(0.0, 0.0, 0.0))
        attractor!.makeFixed()
    }
    
    func panRecognized(panRecognizer: UIPanGestureRecognizer) {
        if(panRecognizer.state == .Ended) {
            let index = highlightedIndex()
            if(index >= 0) {
                selectedItemIndex = index
                self.sendActionsForControlEvents(UIControlEvents.ValueChanged)
            }
            resetHighlights()
        }
        else {
            setFrameInScrollView(scrollView!)
            processTouchLocation(panRecognizer.locationInView(self))
        }
        
    }
    
    func addToScrollView(scrollView: UIScrollView) {
        self.scrollView = scrollView
        scrollView.addSubview(self)
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        setFrameInScrollView(scrollView)
        monitorActiveStatus()
        setAttractorPosition()
    }
    
    func setFrameInScrollView(scrollView: UIScrollView) {
        /*var targetHeight = scrollView.frame.size.height > scrollView.contentSize.height - scrollView.contentInset.top ? scrollView.frame.size.height : scrollView.contentSize.height
        //if the contentSize is smaller than the scrollview
        var offset = /*targetHeight - scrollView.frame.size.height*/ 0 - scrollView.contentOffset.y
        var height = offset >= 0 ? 0 : offset * -1
        var size = CGSizeMake(scrollView.frame.size.width, height)
        var origin = CGPointMake(0, targetHeight - size.height)
        if(offset < 0) {
            origin.y -= offset
        }
        active = offset < -80
        var attractorY: CMTPFloat = -60 - CMTPFloat(offset) / 6
        attractorY = attractorY > -30 ? -30 : attractorY
        attractor!.position.y = attractorY
        frame.size = size
        frame.origin = origin
        self.updateAnchors()
        self.setNeedsDisplay()
        self.setNeedsLayout()*/
        //let lowerEdge = max(scrollView.contentSize.height, scrollView.frame.size.height - scrollView.contentInset.top)
        //let realOffset = scrollView.contentOffset.y - (lowerEdge - scrollView.frame.size.height)
        var origin = CGPointMake(0, lowerEdge())
        var size = CGSizeMake(scrollView.frame.size.width, calculatedOffset())
        frame.size = size
        frame.origin = origin
        self.updateAnchors()
        self.setNeedsDisplay()
        self.setNeedsLayout()
    }
    
    func setAttractorPosition() {
        //active - 30
        if(active) {
            attractor!.position.y = -30
        }
        else {
            attractor!.position.y = -60 + CMTPFloat(calculatedOffset())/4
        }
    }
    
    func monitorActiveStatus() {
        active = calculatedOffset() > 70
    }
    
    func lowerEdge() -> CGFloat {
        return max(scrollView!.contentSize.height, scrollView!.frame.size.height - scrollView!.contentInset.top)
    }
    
    func calculatedOffset() -> CGFloat {
        return scrollView!.contentOffset.y - (lowerEdge() - scrollView!.frame.size.height)
    }
    
    func reloadData() {
        clearItems()
        //addItems()
    }
    
    func draw() {
        if(doTick) {
            particleSystem!.tick(1)
            self.setNeedsDisplay()
            self.setNeedsLayout()
        }
    }
    
    //remove all items
    func clearItems() {
        
    }
    
    //add items
    func addItems() {
        let gravity = CMTPVector3DMake(0.0, 0.0, 0.0);
        for(var i:NSInteger = 0; i < numberOfItems(); i++) {
            let item = createItemForIndex(i)
            items.addObject(item)
            self.insertSubview(item, atIndex: 0)
            //item.particle = particleSystem!.make
        }
    }
    
    func numberOfItems() -> NSInteger {
        return 5
    }
    
    //get data from data source and return a PullToPostItem
    func createItemForIndex(index:NSInteger) -> PullToPostItem {
        let item = PullToPostItem(frame: CGRectMake(0, 0, itemSize, itemSize))
        let anchorPosition = anchorPositionAtIndex(index)
        //apply anchor position
        item.anchor = particleSystem!.makeParticleWithMass(1.0, position: anchorPosition)
        item.anchor!.makeFixed()
        //apply movable particle
        item.particle = particleSystem!.makeParticleWithMass(1.0, position: anchorPosition)
        //apply a spring between the anchor and the moving particle
        item.spring = particleSystem!.makeSpringBetweenParticleA(item.anchor!, particleB: item.particle!, springConstant: 1.0, damping: 0.3, restLength: 0.0)
        //apply attraction between the attractor and the particle
        item.attraction = particleSystem!.makeAttractionBetweenParticleA(item.particle!, particleB: attractor!, strength: 8000, minDistance: 30)
        item.highlighted = false
        return item
    }
    
    func anchorPositionAtIndex(index:NSInteger) -> CMTPVector3D {
        var fNumberOfItems = CGFloat(numberOfItems())
        var containerWidth: CGFloat = fNumberOfItems * itemSize + (fNumberOfItems - 1) * itemMargin
        var minX = (frame.size.width / 2) - (containerWidth / 2)
        var fIndex: CGFloat = CGFloat(index)
        var x = minX + itemSize * fIndex + itemMargin * fIndex
        x = x + itemSize / 2
        return CMTPVector3DMake(CMTPFloat(x), 0, 0)
    }
    
    //set spring positions
    func applySpringPositions() {
        for object in items {
            var item = object as! PullToPostItem
            
        }
    }
    
    func turnOffSprings() {
        for object in items {
            var item = object as! PullToPostItem
            item.spring!.turnOff()
        }
    }
    
    func turnOnSprings() {
        for object in items {
            var item = object as! PullToPostItem
            item.spring!.turnOn()
        }
    }
    
    func turnOffAttractions() {
        for object in items {
            var item = object as! PullToPostItem
            item.attraction!.turnOff()
        }
    }
    
    func turnOnAttractions() {
        for object in items {
            var item = object as! PullToPostItem
            item.attraction!.turnOn()
        }
    }
    
    func updateAnchors() {
        turnOffSprings()
       // turnOffAttractions()
        //doTick = false
        for (index,object) in enumerate(items) {
            var item = object as! PullToPostItem
            //item.particle!.position = anchorPositionAtIndex(index)
            item.anchor!.position = anchorPositionAtIndex(index)
        }
        turnOnSprings()
       // turnOnAttractions()
    }
    
    func resetHighlights() {
        for object in items {
            var item = object as! PullToPostItem
            item.highlighted = false;
        }
    }
    
    func resetSprings() {
        //turnOffSprings()
        //turnOffAttractions()
        for object in items {
            var item = object as! PullToPostItem
            item.spring!.springConstant = 1
            item.spring!.damping = 1
            item.particle!.setMass(0.6)
            item.attraction!.turnOn()
        }
        //turnOnAttractions()
        //turnOnSprings()
    }
    
    func setClosestItem() {
        resetHighlights()
        var closest = closestItem()
        if closest != nil {
            closest!.particle!.setMass(1)
            closest!.spring!.damping = 0.1
            closest!.spring!.springConstant = active ? 0.3 : 0.6
            closest!.highlighted = active
        }
    }
    
    override func beginTrackingWithTouch(touch: UITouch, withEvent event: UIEvent) -> Bool {
        return true
    }
    
    override func continueTrackingWithTouch(touch: UITouch, withEvent event: UIEvent) -> Bool {
        let location = touch.locationInView(self)
        return true
    }
    
    override func endTrackingWithTouch(touch: UITouch, withEvent event: UIEvent) {
        attractor!.position = CMTPVector3DMake(0, -200, 0)
        let index = highlightedIndex()
        resetHighlights()
    }
    
    func processTouchLocation(location: CGPoint) {
        attractor!.position = CMTPVector3DMake(CMTPFloat(location.x), attractor!.position.y, 0)
        //if(active) {
            resetSprings()
            setClosestItem()
        //}
    }
    
    func highlightedIndex() -> NSInteger {
        for (index,object) in enumerate(items) {
            if((object as! PullToPostItem).highlighted) {
                return index
            }
        }
        return -1
    }
    
    func closestItem() -> PullToPostItem? {
        var closest: PullToPostItem?
        var closestDistance: CGFloat = 99999
        for object in items {
            var item = object as! PullToPostItem
            let distance = itemDistanceToAttractor(item)
            if(distance < closestDistance) {
                closest = item
                closestDistance = distance
            }
        }
        return closest
    }
    
    func itemDistanceToAttractor(item:PullToPostItem) -> CGFloat {
        var point1 = item.anchor!.position
        var point2 = attractor!.position
        var vec: CGSize = CGSizeMake(CGFloat(point1.x) - CGFloat(point2.x), CGFloat(point1.y) - CGFloat(point2.y))
        return sqrt(pow(vec.width, 2) + pow(vec.width, 2))
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        applyItemPositions()
        applyShadowLayerPosition()
    }
    
    func applyItemPositions() {
        for object in items {
            var item = object as! PullToPostItem
            var sign: CGFloat = CGFloat(item.particle!.position.x) == 0 ? 1 : abs(CGFloat(item.particle!.position.x))/CGFloat(item.particle!.position.x)
            var offset: CGFloat = CGFloat(item.particle!.position.x) - CGFloat(item.anchor!.position.x)
            var x: CGFloat = CGFloat(item.anchor!.position.x) + (offset/8) * sign
            var y: CGFloat = CGFloat(item.particle!.position.y) + frame.size.height - 30
            //x = x + CGFloat(itemSize) / 2
            //y = y + CGFloat(itemSize) / 2
            var center = CGPointMake(CGFloat(x), CGFloat(y))
            item.center = center
        }
    }
    
    func applyShadowLayerPosition() {
        shadowView.frame = CGRectMake(0, -10, frame.size.width, 10)
    }
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect)
    {
        // Drawing code
    }
    */

}
