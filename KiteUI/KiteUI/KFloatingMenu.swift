//
//  KFloatingMenu.swift
//  KiteUI
//
//  Created by Matteo Koczorek on 31.12.14.
//  Copyright (c) 2014 Matteo Koczorek. All rights reserved.
//

import UIKit

enum KFloatingMenuState {
    case Collapsed, Expanded
}

@objc protocol KFloatingMenuDelegate {
    optional func floatingMenu(floatingMenu: KFloatingMenu, didSelectItemAtIndex index: NSInteger)
    optional func floatingMenuDidChangeState(floatingMenu: KFloatingMenu)
}

class KFloatingMenuItem: UIView {
    
    // MARK: - Config
    var padding: CGFloat = 0
    
    // MARK: - Properties
    var iconView: UIImageView = UIImageView()
    var kIcon: UIImage! {
        didSet {
            //self.imageView.image = self.kIcon
        }
    }
    
    var selected: Bool = false {
        didSet {
            self.contentScale = selected ? 0.91 : 1.0
        }
    }
    var contentScale: CGFloat = 1.0 {
        didSet {
            self.layout()
        }
    }
    var availableSize: CGSize {
        get {
            var width = self.bounds.size.width * self.contentScale
            var height = self.bounds.size.height * self.contentScale
            return CGSizeMake(width, height)
        }
    }
    
    var shadowView: UIImageView = UIImageView()
    private var _circleImage: UIImage?
    var circleImage: UIImage? {
        get {
            if self._circleImage != nil {
                return self._circleImage
            }
            return UIImage(named: "KFloatingMenuCircle.png")
        }
        set {
            self._circleImage = newValue
            self.circleView.image = newValue
        }
    }
    var circleView: UIImageView = UIImageView()
    
    // MARK: - Init
    init(icon: UIImage) {
        super.init(frame: CGRectZero)
        self.setup()
        self.kIcon = icon
        self.iconView.image = icon
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Setup
    func setup() {
        self.setupShadowView()
        self.setupCircleView()
        self.setupIconView()
       // self.clipsToBounds = true
    }
    
    func setupIconView() {
        self.addSubview(self.iconView)
    }
    
    func setupShadowView() {
        self.addSubview(self.shadowView)
        self.shadowView.image = UIImage(named: "KFloatingMenuShadow.png")
    }
    
    func setupCircleView() {
        self.addSubview(self.circleView)
        self.circleView.image = self.circleImage
    }
    
    // MARK: - Layout
    
    func contentFrame() -> CGRect {
        var width = self.availableSize.width
        var height = self.availableSize.height
        var x = self.bounds.size.width / 2 - width / 2
        var y = self.bounds.size.height / 2 - height / 2
        return CGRectMake(x, y, width, height)
    }
    
    func layoutShadowView() {
        var width = self.availableSize.width
        var height = self.availableSize.height
        var x = self.bounds.size.width / 2 - width / 2
        var y = self.bounds.size.height / 2 - height / 2
        self.shadowView.frame = self.contentFrame()
    }
    
    func layoutIconView() {
        var width = self.availableSize.width - self.padding * 2
        var height = self.availableSize.height - self.padding * 2
        var x = self.bounds.size.width / 2 - width / 2
        var y = self.bounds.size.height / 2 - height / 2
        self.iconView.frame = CGRectMake(x, y, width, height)
    }
    
    func layoutCircleView() {
        self.circleView.frame = self.contentFrame()
    }
    
    func layout() {
        self.layoutIconView()
        self.layoutCircleView()
        self.layoutShadowView()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layout()
    }
    
}

class KFloatingMenu: UIControl {
    
    // MARK: - Config
    var itemSize: CGFloat = 64
    var itemSpacing: CGFloat = 10
    
    // MARK: - Properties
    weak var delegate: KFloatingMenuDelegate?
    var kState: KFloatingMenuState = .Collapsed {
        didSet {
            self.layoutItems()
            self.delegate?.floatingMenuDidChangeState?(self)
        }
    }
    
    var rootItem: KFloatingMenuItem!
    var items: [KFloatingMenuItem] = []
    var allItems: [KFloatingMenuItem]  {
        get {
            var items = NSArray(array: self.items) as! [KFloatingMenuItem]
            items.append(self.rootItem)
            return items
        }
    }
    var icons: [UIImage] = [] {
        didSet {
            self.setupItems()
        }
    }
    
    // MARK: - GestureRecognizers
    var tapGestureRecognizer: UITapGestureRecognizer!
    var pressGestureRecognizer: UILongPressGestureRecognizer!
    
    // MARK: - Init
    init() {
        super.init(frame: CGRectZero)
        self.setup()
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    // MARK: - Setup 
    func setup() {
        self.setupRootItem()
        self.setupGestureRecognizers()
    }
    
    func setupGestureRecognizers() {
        //self.tapGestureRecognizer = UITapGestureRecognizer(target: self, action: "handleTap:")
        //self.addGestureRecognizer(self.tapGestureRecognizer)
        self.pressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: "handlePress:")
        self.pressGestureRecognizer.minimumPressDuration = 0.0001
        self.addGestureRecognizer(self.pressGestureRecognizer)
    }
    
    // MARK: - Items
    func setupRootItem() {
        self.rootItem = KFloatingMenuItem(icon: UIImage(named: "KFloatingMenuPlusIcon.png")!)
        self.rootItem.circleImage = UIImage(named: "KFloatingMenuRootCircle.png")
        self.addSubview(self.rootItem)
    }
    
    func clearItems() {
        for item in self.items {
            item.removeFromSuperview()
        }
        self.items = []
    }
    
    func setupItems() {
        self.clearItems()
        for (index, icon) in enumerate(self.icons) {
            var itemView = KFloatingMenuItem(icon: icon)
            //self.addSubview(itemView)
            self.insertSubview(itemView, atIndex: 0)
            self.items.append(itemView)
        }
        self.setState(self.kState, animated: false)
    }
    
    // MARK: - Helpers
    func transformForItem() -> CGAffineTransform {
        if self.kState == .Collapsed {
            var angle: CGFloat = -45.0 / 180.0 * CGFloat(M_PI)
            var transform = CGAffineTransformMakeRotation(angle)
            return transform
        }
        return CGAffineTransformIdentity
    }
    
    func frameForItemAtIndex(index: NSInteger) -> CGRect {
        if self.kState == .Collapsed {
            return CGRectMake(0, 0, self.itemSize, self.itemSize)
        }
        else if self.kState == .Expanded {
            var x: CGFloat = (self.itemSize + self.itemSpacing) * CGFloat(index)
            var y: CGFloat = 0
            return CGRectMake(x, y, self.itemSize, self.itemSize)
        }
        return CGRectZero
    }
    
    func shadowAlphaForItem() -> CGFloat {
        switch self.kState {
        case .Collapsed:
            return 0
        case .Expanded:
            return 1
        default:
            return 1
        }
    }
    
    func itemAtLocation(location: CGPoint) -> KFloatingMenuItem? {
        if CGRectContainsPoint(self.rootItem.frame, location) {
            return self.rootItem
        }
        for item in self.items {
            if CGRectContainsPoint(item.frame, location) {
                return item
            }
        }
        return nil
    }
    
    func indexForItem(item: KFloatingMenuItem) -> NSInteger {
        for (index,anItem) in enumerate(self.items) {
            if anItem == item {
                return index
            }
        }
        return -1
    }
    
    func setState(state: KFloatingMenuState, animated: Bool) {
        self.stateWillChangeTo(state)
        if animated {
            UIView.animateWithDuration(0.5, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.01, options: nil, animations: { () -> Void in
                self.kState = state
            }, completion: { (completed) -> Void in
                self.invalidateIntrinsicContentSize()
                self.stateDidChangeTo(state)
            })
        }
        else {
            self.kState = state
            self.stateDidChangeTo(state)
            self.invalidateIntrinsicContentSize()
        }
    }
    
    func toggleState(animated: Bool) {
        var newState: KFloatingMenuState!
        if self.kState == .Collapsed {
            newState = .Expanded
        }
        else {
            newState = .Collapsed
        }
        self.setState(newState, animated: animated)
    }
    
    func stateWillChangeTo(state: KFloatingMenuState) {
        if state == .Expanded {
            for item in self.items {
                item.alpha = 1
            }
        }
    }
    
    func stateDidChangeTo(state: KFloatingMenuState) {
        if state == .Collapsed {
            for item in self.items {
                item.alpha = 0
            }
        }
    }
    
    
    
    // MARK: - Layout Icons
    func layoutItems() {
        self.layoutRootItem()
        
        for (index, item) in enumerate(self.items) {
            item.transform = CGAffineTransformIdentity
            item.frame = self.frameForItemAtIndex(index+1)
            item.transform = self.transformForItem()
            item.shadowView.alpha = self.shadowAlphaForItem()
        }
    }
    
    func layoutRootItem() {
        self.rootItem.transform = CGAffineTransformIdentity
        self.rootItem.frame = self.frameForItemAtIndex(0)
        self.rootItem.transform = self.transformForItem()
    }
    
    // MARK: - UIView
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layoutItems()
    }
    
    // MARK: - Gesture Handlers
    func handleTap(sender: UIGestureRecognizer) {
        var location = sender.locationInView(self)
        var item = self.itemAtLocation(location)
        if item != nil {
            if item! == self.rootItem {
                self.toggleState(true)
            }
            else {
                var index = self.indexForItem(item!)
                if index >= 0 {
                    self.delegate?.floatingMenu?(self, didSelectItemAtIndex: index)
                    //self.setState(.Collapsed, animated: true)
                }
            }
        }
    }
    
    func handlePress(sender: UIPanGestureRecognizer) {
        if sender.state == .Began {
            let location = sender.locationInView(self)
            for item in self.allItems {
                if CGRectContainsPoint(item.frame, location) {
                    item.selected = true
                }
            }
        }
        if sender.state == .Ended {
            self.handleTap(sender)
            for item in self.allItems {
                item.selected = false
            }
        }
    }
    
    override func intrinsicContentSize() -> CGSize {
        var width: CGFloat!
        if self.kState == .Expanded {
            width = self.itemSize + CGFloat(self.items.count) * (self.itemSize + self.itemSpacing)
        }
        else {
            width = self.itemSize
        }
        return CGSizeMake(width, self.itemSize)
    }
    
    // MARK: - UIControl
    /*override func beginTrackingWithTouch(touch: UITouch, withEvent event: UIEvent) -> Bool {
        super.beginTrackingWithTouch(touch, withEvent: event)
        let location = touch.locationInView(self)
        for item in self.allItems {
            if CGRectContainsPoint(item.frame, location) {
                item.selected = true
            }
        }
        return true
    }
    
    override func endTrackingWithTouch(touch: UITouch, withEvent event: UIEvent) {
        super.endTrackingWithTouch(touch, withEvent: event)
        for item in self.allItems {
            item.selected = false
        }
    }*/
    /*
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        super.touchesBegan(touches, withEvent: event)
        let touch = touches.allObjects.first! as UITouch
        let location = touch.locationInView(self)
        for item in self.allItems {
            if CGRectContainsPoint(item.frame, location) {
                item.selected = true
            }
        }
    }
    
    override func touchesEnded(touches: NSSet, withEvent event: UIEvent) {
        super.touchesEnded(touches, withEvent: event)
        for item in self.allItems {
            item.selected = false
        }
    }*/
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
