//
//  MKSheetViewControllerPresenter.swift
//  Segue
//
//  Created by Matteo Koczorek on 31.08.14.
//  Copyright (c) 2014 com.matteo-koczorek. All rights reserved.
//

import UIKit

class MKSheetViewControllerPresenter: MKViewControllerPresenter {
    
    var dimmer: UIView?
    var dimmerAlpha: CGFloat = 0.7
    
    override func setupContainer() {
        super.setupContainer()
        self.applyDimmer()
    }
    
    func applyDimmer() {
        dimmer = UIView()
        dimmer!.frame = container!.frame
        dimmer!.backgroundColor = UIColor.blackColor()
        container!.insertSubview(dimmer!, atIndex: 0)
    }
    
    override func show(completion:()->()) {
        var frame = destinationViewControllerFrame()
        var startFrame = destinationViewControllerFrameDisplaced()
        presentedView!.frame = startFrame
        
        dimmer!.alpha = 0
        UIView.animateWithDuration(0.3, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
            self.presentedView!.frame = frame
            self.dimmer!.alpha = self.dimmerAlpha
            }, completion: { completed in
                completion()
        })
    }
    
    override func hide(completion:()->()) {
        UIView.animateWithDuration(0.3, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
            self.presentedView!.frame = self.destinationViewControllerFrameDisplaced()
            self.dimmer!.alpha = 0
            }, completion: { completed in
                self.dimmer!.removeFromSuperview()
                self.dimmer = nil
                completion()
        })
    }
    
    func destinationViewControllerFrameDisplaced() -> CGRect {
        var frame = destinationViewControllerFrame()
        frame.origin = CGPointMake(frame.origin.x, container!.frame.size.height)
        return frame
    }
    
    
    func destinationViewControllerFrame() -> CGRect {
        var x: CGFloat = container!.frame.size.width/2 - _size().width/2
        var y: CGFloat = container!.frame.size.height/2 - _size().height/2
        return CGRectMake(x, y, _size().width, _size().height)
    }
    
    
}
