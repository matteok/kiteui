//
//  MKTopSheetViewControllerPresenter.swift
//  Piles
//
//  Created by Matteo Koczorek on 19.12.14.
//  Copyright (c) 2014 Matteo Koczorek. All rights reserved.
//

import UIKit

class MKTopSheetViewControllerPresenter: MKSheetViewControllerPresenter {
   
    var topOffset: CGFloat = 30
    
    override func destinationViewControllerFrameDisplaced() -> CGRect {
        var frame = destinationViewControllerFrame()
        frame.origin = CGPointMake(frame.origin.x, -frame.size.height)
        return frame
    }
    
    
    override func destinationViewControllerFrame() -> CGRect {
        var x: CGFloat = container!.frame.size.width/2 - _size().width/2
        var y: CGFloat = topOffset
        return CGRectMake(x, y, _size().width, _size().height)
    }
    
    override func show(completion:()->()) {
        var frame = destinationViewControllerFrame()
        var startFrame = destinationViewControllerFrameDisplaced()
        presentedView!.frame = startFrame
        
        dimmer!.alpha = 0
        UIView.animateWithDuration(0.7, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.01, options: nil, animations: { () -> Void in
            self.presentedView!.frame = frame
            self.dimmer!.alpha = self.dimmerAlpha
        }) { (completed) -> Void in
            completion()
        }
        
    
    }
    
    override func hide(completion:()->()) {
        UIView.animateWithDuration(0.8, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.01, options: nil, animations: { () -> Void in
            self.presentedView!.frame = self.destinationViewControllerFrameDisplaced()
            self.dimmer!.alpha = 0
            }, completion: { completed in
                self.dimmer!.removeFromSuperview()
                self.dimmer = nil
                completion()
        })
    }
    
}
