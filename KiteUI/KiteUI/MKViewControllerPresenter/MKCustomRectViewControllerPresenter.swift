//
//  MKCustomRectViewControllerPresenter.swift
//  MKViewControllerPresenter
//
//  Created by Matteo Koczorek on 23.09.14.
//  Copyright (c) 2014 com.matteo-koczorek. All rights reserved.
//

import UIKit

class MKCustomRectViewControllerPresenter: MKViewControllerPresenter {
   
    var rect = CGRectZero
    var tapGestureRecognizer: UITapGestureRecognizer?
    
    override func setupContainer() {
        super.setupContainer()
        applyGestureRecognizer()
    }
    
    func applyGestureRecognizer() {
        tapGestureRecognizer = UITapGestureRecognizer(target: self, action: "tapRecognized:")
        container!.addGestureRecognizer(tapGestureRecognizer!)
    }
    
    func tapRecognized(sender: UITapGestureRecognizer) {
        var location = sender.locationInView(container)
        if(!CGRectContainsPoint(rect, location)) {
            dismiss()
        }
    }
    
    override func show(completion:()->()) {
        presentedView!.frame = rect
        presentedView!.alpha = 0
        UIView.animateWithDuration(0.3, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
            self.presentedView!.alpha = 1
            }, completion: { completed in
                completion()
        })
        /*var frame = destinationViewControllerFrame()
        var startFrame = destinationViewControllerFrameDisplaced()
        presentedView!.frame = startFrame
        
        dimmer!.alpha = 0
        UIView.animateWithDuration(0.3, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
            self.presentedView!.frame = frame
            self.dimmer!.alpha = self.dimmerAlpha
            }, completion: { completed in
                completion()
        })*/
    }
    
    override func hide(completion:()->()) {
        UIView.animateWithDuration(0.3, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
            self.presentedView!.alpha = 0
            }, completion: { completed in
                completion()
        })
        /* UIView.animateWithDuration(0.3, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
            self.presentedView!.frame = self.destinationViewControllerFrameDisplaced()
            self.dimmer!.alpha = 0
            }, completion: { completed in
                self.dimmer!.removeFromSuperview()
                self.dimmer = nil
                completion()
        })*/
    }
    
}
