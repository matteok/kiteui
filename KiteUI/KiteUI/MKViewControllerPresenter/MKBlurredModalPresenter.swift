//
//  MKBlurredModalPresenter.swift
//  Segue
//
//  Created by Matteo Koczorek on 31.08.14.
//  Copyright (c) 2014 com.matteo-koczorek. All rights reserved.
//

import UIKit

class MKBlurredModalPresenter: MKViewControllerPresenter {
   
    
    var blurView: UIView?
    var insets: UIEdgeInsets = UIEdgeInsets(top: 68, left: 10, bottom: 10, right: 10)
    var doneButton: UIButton?
    var doneButtonTitle = "Done"
    var titleLabel: UILabel?
    var title: String = "" {
        didSet {
            if(titleLabel != nil) {
                titleLabel!.text = title
                applyTitleLabelFrame()
            }
        }
    }
    
    override var presentedViewController:UIViewController? {
        didSet {
            if(presentedViewController != nil) {
                if(presentedViewController!.title != nil) {
                    title = presentedViewController!.title!
                }
            }
        }
    }
    
    override func setupContainer() {
        super.setupContainer()
        applyBlurView()
        applyHeader()
    }
    
    func applyHeader() {
        applyDoneButton()
        applyTitleLabel()
    }
    
    func applyTitleLabel() {
        titleLabel = UILabel()
        titleLabel!.text = title
        titleLabel!.textColor = UIColor.whiteColor()
        container!.addSubview(titleLabel!)
        applyTitleLabelFrame()
    }
    
    func applyTitleLabelFrame() {
        titleLabel!.sizeToFit()
        var frame = titleLabel!.frame
        titleLabel!.frame = CGRectMake(insets.left + 20, 25, frame.size.width, 33)
    }
    
    func applyDoneButton() {
        doneButton = UIButton()
        container!.addSubview(doneButton!)
        doneButton!.setTitle(doneButtonTitle, forState: .Normal)
        doneButton!.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1)
        doneButton!.layer.cornerRadius = 3
        doneButton!.layer.borderColor = UIColor.whiteColor().CGColor
        doneButton!.layer.borderWidth = 1
        doneButton!.addTarget(self, action: "doneButtonPressed:", forControlEvents: UIControlEvents.TouchUpInside)
        applyDoneButtonFrame()
    }
    
    func applyDoneButtonFrame() {
        doneButton!.sizeToFit()
        var padding: CGFloat = 10.0;
        var frame = doneButton!.frame
        frame.size = CGSizeMake(doneButton!.frame.size.width + padding * 2, doneButton!.frame.size.height)
        doneButton!.frame = CGRectMake(container!.frame.size.width - frame.size.width - insets.right, 25, frame.size.width, 33)
    }
    
    func doneButtonPressed(sender:UIButton) {
        dismiss()
    }
    
    override func show(completion: () -> ()) {
        doneButton!.alpha = 0
        blurView!.alpha = 0
        titleLabel!.alpha = 0
        presentedView!.frame = destinationViewControllerFrameDisplaces()
        UIView.animateWithDuration(0.3, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
            self.blurView!.alpha = 1
            self.doneButton!.alpha = 1
            self.titleLabel!.alpha = 1
            self.presentedView!.frame = self.destinationViewControllerFrame()
            }, completion: { completed in
                completion()
        })
    }
    
    
    override func hide(completion: () -> ()) {
        UIView.animateWithDuration(0.3, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut,
            animations: {
                self.blurView!.alpha = 0
                self.doneButton!.alpha = 0
                self.titleLabel!.alpha = 0
                self.presentedView!.frame = self.destinationViewControllerFrameDisplaces()
            }, completion: { completed in
                self.blurView = nil
                self.doneButton = nil
                self.titleLabel = nil
                completion()
        })
    }
    
    func destinationViewControllerFrame() -> CGRect {
        return CGRectMake(insets.left, insets.top, _size().width, _size().height)
    }
    
    func destinationViewControllerFrameDisplaces() -> CGRect {
        var frame = CGRectMake(insets.left, insets.top, _size().width, _size().height)
        frame.origin = CGPointMake(frame.origin.x, self.container!.frame.size.height)
        return frame
    }
    
    func captureSourceView() {
        var sourceViewDuplicate = self.presentingViewController!.view.snapshotViewAfterScreenUpdates(true)
        sourceViewDuplicate.frame.size = self.presentingViewController!.view.frame.size
        blurView!.addSubview(sourceViewDuplicate)
    }
    
    func SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(version: NSString) -> Bool {
        return UIDevice.currentDevice().systemVersion.compare(version as String,
            options: NSStringCompareOptions.NumericSearch) != NSComparisonResult.OrderedAscending
    }
    
    func applyBlurView() {
        blurView = UIView()
        blurView!.frame.size = container!.frame.size
        container!.insertSubview(blurView!, atIndex: 0)
        if(self.SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO("8.0")) {
            captureSourceView()
            var blur:UIBlurEffect = UIBlurEffect(style: UIBlurEffectStyle.Dark)
            var blurEffectView = UIVisualEffectView (effect: blur)
            blurEffectView.frame = container!.frame
            blurView!.addSubview(blurEffectView)
        }
        else {
            blurView!.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.85)
        }
    }
    
    override func _size() -> CGSize {
        return CGSize(width: container!.frame.size.width - self.insets.left - self.insets.right, height: self.container!.frame.size.height-self.insets.top-self.insets.bottom)
    }
    
}
