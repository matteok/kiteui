//
//  MKSegmentSlider.swift
//  MKSegmentSlider
//
//  Created by Matteo Koczorek on 26.08.14.
//  Copyright (c) 2014 Matteo Koczorek. All rights reserved.
//
/*
* The segment slider implements a slideable variant of a UISegmentedControl
* It can have of both images and strings as segment titles
* While the handle is covering a title it is being displayed in the highlightColor this is achieved by duplicating
* the view that contains the segments using masking to color it in the highlightColor and mask it using a maskLayer
* that duplicates size and position of the handle.
*/

import UIKit

class MKSegmentSliderSegment: UIView {
    
    //public
    var title: NSString? {
        didSet {
            _titleLabel.text = title as? String
            self.setFrames()
        }
    }
    var image: UIImage? {
        didSet {
            _imageView.image = image
            self.setFrames()
        }
    }
    var imageInset = CGSizeMake(3, 3)
    override var frame: CGRect {
        didSet {
            setFrames()
        }
    }
    
    var textColor: UIColor = UIColor.whiteColor() {
        didSet {
            _titleLabel.textColor = textColor
        }
    }
    
    //private
    private var _titleLabel:UILabel = UILabel()
    private var _imageView: UIImageView = UIImageView()
    
    init() {
        super.init(frame: CGRectZero)
        construct()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        construct()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        construct()
    }
    
    //return a new segment instance with identical properties
    func duplicate() -> MKSegmentSliderSegment {
        var double = MKSegmentSliderSegment(frame: frame)
        double.title = title
        double.image = image
        return double
    }
    
    
    //internal methods
    private func construct() {
        _titleLabel.textColor = textColor
        _titleLabel.textAlignment = .Center
        addSubview(_titleLabel)
        addSubview(_imageView)
        self.setFrames()
    }
    
    private func setFrames() {
        setTitleFrame()
        setImageViewFrame()
    }
    
    private func setTitleFrame() {
        _titleLabel.frame = CGRect(x:0,y:0,width:self.frame.size.width, height:self.frame.size.height)
    }
    
    //set imageViewFrame either by height or by width
    private func setImageViewFrame() {
        if image != nil {
            var width: CGFloat?
            var height: CGFloat?
            if self.imageViewShouldLayoutByHeight() {
                height = self.maxImageHeight()
                let ratio: CGFloat = height! / image!.size.height
                width = image!.size.width * ratio
            }
            else {
                width = self.maxImageWidth()
                let ratio: CGFloat = width! / image!.size.width
                height = image!.size.height * ratio
            }
            _imageView.frame = CGRectMake(frame.width / 2 - width! / 2, frame.height / 2 - height! / 2, width!, height!)
        }
    }
    
    private func imageViewShouldLayoutByHeight() -> Bool {
        return image!.size.width / image!.size.height < self.maxImageWidth() / maxImageHeight()
    }
    
    private func maxImageWidth() -> CGFloat {
        return frame.size.width - imageInset.width * 2
    }
    
    private func maxImageHeight() -> CGFloat {
        return frame.size.height - imageInset.height * 2
    }
    
    
}

class MKSegmentSliderHandle: CALayer {
    override init() {
        super.init()
        self.construct()
    }
    
    override init(layer: AnyObject!) {
        super.init(layer: layer)
        self.construct()
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.construct()
    }
    
    
    func construct() {
        self.cornerRadius = 4;
        self.backgroundColor = UIColor.whiteColor().CGColor
    }
    
}

class MKSegmentSlider: UIControl {
    
    //public
    var borderRadius:CGFloat = 4
    var handleInset = CGSizeMake(2, 2)
    var selectedSegmentIndex: NSInteger {
        get {
            return _selectedSegmentIndex
        }
        set {
            if(newValue >= 0 && newValue < self.getNumberOfSegments()) {
                _selectedSegmentIndex = newValue
                self.sendActionsForControlEvents(.ValueChanged)
                self.snap()
            }
        }
    }
    
    var numberOfSegments: NSInteger {
        get {
            return _segments!.count
        }
    }
    override var frame: CGRect {
        didSet {
            self.layout()
        }
    }
    
    //private
    private var _selectedSegmentIndex: NSInteger = 0
    
    private var _segmentsView = UIView()
    
    private var _segments: Array<MKSegmentSliderSegment>?
    
    private var _titles: Array<NSString>?
    private var _images: Array<UIImage>?
    private var _handle = MKSegmentSliderHandle()
    
    //highlight
    private var _highlightedSegmentsView = UIView()
    private var _highlightedSegmentsViewContainer = UIView()
    private var _highlightedSegmentsMask = MKSegmentSliderHandle()
    
    private var _handleFrame: CGRect? {
        didSet {
            _handle.frame = _handleFrame!
            _highlightedSegmentsMask.frame = _handleFrame!
        }
    }
    
    
    //interaction 
    
    //flags
    private var _didTouchHandle:Bool = false
    
    //stored values
    private var _initialTouchLocation: CGPoint?
    private var _handleStartLocation: CGPoint?
    

    //initializers
    init() {
        super.init(frame: CGRectZero)
        self.construct()
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.construct()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.construct()
    }
    
    init(titles:Array<String>) {
        super.init(frame: CGRectZero)
        _titles = titles
        self.construct()
    }
    
    init(images:Array<UIImage>) {
        super.init(frame: CGRectZero)
        _images = images
        self.construct()
    }
    
    
    //internal
    
    private func construct() {
        self.opaque = false
        
        _segmentsView.userInteractionEnabled = false
        
        self.addSubview(_segmentsView)
        self.layer.addSublayer(_handle)
        
        self.setLayerProperties()
        self.initHighlight()
        self.createSegments()
        self.layout()
    }
    
    private func setLayerProperties() {
        self.layer.cornerRadius = borderRadius
        self.layer.masksToBounds = true
        //self.layer.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3).CGColor
        self.layer.borderWidth = 0.1;
        self.layer.borderColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.8).CGColor
    }
    
    //remove everything
    private func clear() {
        
    }
    
    //create segmntes
    private func createSegments() {
        var segments: Array<MKSegmentSliderSegment> = []
        for(var i:NSInteger = 0; i < self.getNumberOfSegments(); i++) {
            
            let segment = MKSegmentSliderSegment()
            segment.title = self.title(atIndex: i)
            segment.image = self.image(atIndex: i)
            segments.append(segment)
            _segmentsView.addSubview(segment)
            
            //debug
            //segment.backgroundColor = UIColor.greenColor()
        }
        _segments = segments
    }
    
    //layout everything
    private func layout() {
        applySegmentsViewFrame()
        applySegmentFrames()
        applyHandleFrame()
        applyHighlightViewProperties()
    }
    
    
    override func observeValueForKeyPath(keyPath: String, ofObject object: AnyObject, change: [NSObject : AnyObject], context: UnsafeMutablePointer<Void>) {
        if(keyPath == "frame") {
        }
    }
    
    
    //apply segment frames
    private func applySegmentFrames() {
        if(_segments != nil) {
            for (index, segment:MKSegmentSliderSegment) in enumerate(_segments!) {
                segment.frame = self.frameForSegment(atIndex: index)
            }
        }
    }
    
    //apply handle frame using segment size and handleInset
    private func applyHandleFrame() {
        _handleFrame = self.handleFrame(atIndex: selectedSegmentIndex)
    }
    
    private func handleFrame(atIndex  index: NSInteger) -> CGRect {
        var width = self.segmentSize().width - handleInset.width * 2
        var height = self.segmentSize().height - handleInset.height * 2
        return CGRectMake(self.segmentSize().width * CGFloat(index) + handleInset.width, handleInset.height, width, height)
    }
    
    
    //set frame for segmentsView and highlightedSegmentsView
    private func applySegmentsViewFrame() {
        var frame:CGRect = CGRectZero
        frame.size = self.frame.size
        _segmentsView.frame = frame
        _highlightedSegmentsView.frame = frame
    }
    
    private func frameForSegment(atIndex index:NSInteger) -> CGRect {
        return CGRectMake(self.segmentSize().width * CGFloat(index), 0, self.segmentSize().width, self.segmentSize().height)
    }
    
    private func segmentSize() -> CGSize {
        if(self.getNumberOfSegments() == 0) {
            return CGSizeZero
        }
        return CGSizeMake(self.frame.size.width / CGFloat(self.getNumberOfSegments()), self.frame.size.height)
    }
    
    private func title(atIndex index:NSInteger) -> NSString? {
        if _titles == nil {
            return nil
        }
        else {
            if index < _titles!.count {
                return _titles![index] as NSString
            }
            else {
                return nil
            }
        }
    }
    
    private func image(atIndex index:NSInteger) -> UIImage? {
        if _images == nil {
            return nil
        }
        else {
            if index < _images!.count {
                return _images![index] as UIImage
            }
            else {
                return nil
            }
        }
    }
    
    //get the number of segments from either image amount or title amount
    private func getNumberOfSegments() -> NSInteger {
        if _titles==nil && _images==nil {
            return 0
        }
        else if _titles != nil && _images == nil {
            return _titles!.count
        }
        else if _titles == nil && _images != nil {
            return _images!.count
        }
        else {
            if _titles!.count > _images!.count {
                return _titles!.count
            }
            else {
                return _images!.count
            }
        }
        
    }
    
    private func segmentIndexForOffset(offset:CGFloat) -> NSInteger {
        if(self.segmentSize().width == 0) {
            return 0
        }
        return NSInteger(offset / self.segmentSize().width)
    }
    
    private func resetFlags() {
        _didTouchHandle = false
    }
    
    private func bound(value: CGFloat, min: CGFloat, max:CGFloat) -> CGFloat {
        if(value <= max && value >= min) {
            return value
        }
        else if(value < min) {
            return min
        }
        else {
            return max
        }
    }
    
    //animate handle to current index position
    private func snap() {
        CATransaction.begin()
        CATransaction.setAnimationDuration(0.2)
        //self.setHandleFrame()
        self.applyHandleFrame()
        CATransaction.commit()
    }
    
    //interaction
    override func beginTrackingWithTouch(touch: UITouch, withEvent event: UIEvent) -> Bool {
        let touchLocation = touch.locationInView(self)
        _initialTouchLocation = touchLocation
        _handleStartLocation = _handle.frame.origin
        if(CGRectContainsPoint(_handle.frame, touchLocation)) {
            _didTouchHandle = true
        }
        
        return true
    }
    
    override func continueTrackingWithTouch(touch: UITouch, withEvent event: UIEvent) -> Bool {
        let touchLocation = touch.locationInView(self)
        
        if(_didTouchHandle) {
            let delta: CGFloat = touchLocation.x - _initialTouchLocation!.x
            var frame: CGRect = _handle.frame
            var x: CGFloat = _handleStartLocation!.x + delta
            
            //bound handle frame to left and rightmost position so it  doesn't leave the bounds of the control
            var leftMostFrame = self.handleFrame(atIndex: 0)
            var rightMostFrame = self.handleFrame(atIndex: self.getNumberOfSegments()-1)
            x = self.bound(x, min: leftMostFrame.origin.x, max: rightMostFrame.origin.x)
            frame.origin = CGPointMake(x, _handleStartLocation!.y)
            
            CATransaction.begin()
            CATransaction.setValue(kCFBooleanTrue as AnyObject, forKey: kCATransactionDisableActions)
            //_handle.frame = frame
            _handleFrame = frame
            CATransaction.commit()
        }
        
        return true
    }
    
    override func endTrackingWithTouch(touch: UITouch, withEvent event: UIEvent) {
        let touchLocation = touch.locationInView(self)
        
        //touch outside handle = select segment
        if(!_didTouchHandle) {
            selectedSegmentIndex = self.segmentIndexForOffset(touchLocation.x)
        }
        else {
            let handleCenter = _handle.frame.origin.x + _handle.frame.size.width/2
            selectedSegmentIndex = self.segmentIndexForOffset(handleCenter)
            self.snap()
        }
        
        self.resetFlags()
    }
    
    
    // Graphics
    override func drawRect(rect: CGRect)
    {
        let context = UIGraphicsGetCurrentContext()
        CGContextClearRect(context, rect);
        let colorspace = CGColorSpaceCreateDeviceRGB()
        var startColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2).CGColor
        var middleColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1).CGColor
        var endColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.0).CGColor
        let gradient = CGGradientCreateWithColors(colorspace, [startColor, endColor], [0.0,1.0])
        var startPoint = CGPoint(x: self.frame.size.width/2, y: 0);
        var endPoint = CGPoint(x: self.frame.size.width/2, y: self.frame.size.height);
        CGContextDrawLinearGradient(context , gradient, startPoint, endPoint, 0)
    }
    

    
    
    //highlight methods
    func initHighlight() {
        _highlightedSegmentsViewContainer.addSubview(_highlightedSegmentsView)
        self.addSubview(_highlightedSegmentsViewContainer)
        
        _highlightedSegmentsView.userInteractionEnabled = false
        _highlightedSegmentsViewContainer.userInteractionEnabled = false
        _highlightedSegmentsView.backgroundColor = UIColor.redColor()
    }
    
    private func applyHighlightViewProperties() {
        _highlightedSegmentsViewContainer.frame.size = frame.size
        _highlightedSegmentsView.frame = _segmentsView.frame
        var maskView = self.copySegmentsView()
        maskView.frame = CGRectMake(0, 0, _segmentsView.frame.size.width, _segmentsView.frame.size.height)
        _highlightedSegmentsViewContainer.addSubview(maskView)
        _highlightedSegmentsView.layer.mask = maskView.layer
        _highlightedSegmentsViewContainer.layer.mask = _highlightedSegmentsMask
    }
    
    private func copySegmentsView() -> UIView {
        var copy = UIView(frame: _segmentsView.frame)
        for subview in _segmentsView.subviews as! [UIView] {
            if(subview.isKindOfClass(MKSegmentSliderSegment)) {
                var segment = (subview as! MKSegmentSliderSegment).duplicate()
                copy.addSubview(segment)
            }
        }
        return copy
    }
    //end highlight methods
}
