//
//  SegmentSliderViewController.swift
//  KiteUI
//
//  Created by Matteo Koczorek on 27.10.14.
//  Copyright (c) 2014 Matteo Koczorek. All rights reserved.
//

import UIKit

class SegmentSliderViewController: UIViewController, UIScrollViewDelegate {

    var segmentSlider: BottomBarSegmentedControl?
    
    @IBOutlet weak var scrollView: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()
        println("jojojo")
        //segmentSlider = BottomBarSegmentedControl(titles: ["messages", "story", "organizer"])
        
        var images = [UIImage(named: "KFloatingMenuPlusIcon.png")!,UIImage(named: "KFloatingMenuPlusIcon.png")!,UIImage(named: "KFloatingMenuPlusIcon.png")!];
        segmentSlider = BottomBarSegmentedControl(images: images)
        segmentSlider!.backgroundColor = UIColor.orangeColor()
        
        segmentSlider!.frame = CGRectMake(0, 64, self.view.frame.size.width, 40)
        self.view.addSubview(segmentSlider!)
       // segmentSlider!.backgroundColor = UIColor(red: 11.0/255.0, green: 116.0/255.0, blue: 191.0/255.0, alpha: 1.0)
        
        scrollView.contentSize.width = self.view.frame.size.width*3
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let percent = scrollView.contentOffset.x / (scrollView.contentSize.width - scrollView.frame.size.width)
        segmentSlider!.moveToPercent(percent)
    }
   

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
